Evolucion.Views.MainMenu = Backbone.View.extend({
	events:{
		"click ul > li": "showSection"
	},
	initialize : function(node){
		
		this.$el = node;

		var self = this;

	},
	showSection : function(e){
		e.preventDefault();
		e.stopPropagation();

		var self = this;
		
		self.$el.find("#MenuMaster").find("li").removeClass("active");;
		

		var btnCurrentTarget = $(e.currentTarget);
		btnCurrentTarget.addClass("active");
		var menu = btnCurrentTarget.attr("data-Menu");
		this.$el.find("#balanceText").show();
		
		switch(menu){
			case "sale" :
				Backbone.history.navigate("/",{trigger:true});
				break;
			case "services" :
				Backbone.history.navigate("/services",{trigger:true});
				break;
			case "insurance" :
				this.$el.find("#balanceText").hide(50,function(){
					Backbone.history.navigate("/insurance",{trigger:true});
				});
				break;
			case "reports" :
				this.$el.find("#balanceText").hide(50,function(){
					Backbone.history.navigate("/reports",{trigger:true});
				});
				break;
		}
	}	
});
