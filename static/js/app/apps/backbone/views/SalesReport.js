Evolucion.Views.SalesReports = Backbone.View.extend({
	events:{
		"click #get-sales-rep": "GetSalesReport"
	},
	initialize : function(node){
		this.$el = node;
	},
	GetSalesReport : function(e){
		e.preventDefault();
		var csrftoken = getCookie('csrftoken');
		var date = $("#date-sales-rep").val();
		$.ajax({
			type: 'POST',
			url: 'sales.report/',
			async: true,
			data: {
				date : date,
				csrfmiddlewaretoken: csrftoken
			},
			success: function(data){
				data = JSON.parse(data);
				if (data.Response == "1"){
					var body_table = $("#table-reports > tbody");
					$("#total-reports").html("$ " + parseFloat(data.Total).toFixed(2));					
					body_table.html("");
					data.SalesDetail.forEach(function(obj) {
						var row = tpl.tr({
							number:obj['number'],
							folio:obj['folio'],
							amount:obj['amount'],
							carrier:obj['carrier']
						});
						body_table.append(row);
					});
				}else{
					console.log(data.Description);
					message.find("strong").html("Error al cagar la información.");
					message.removeClass("alert-info").removeClass("alert-success").addClass("alert-danger");
				}
			},
			error:function(err,e){
			}
		});
	}
});
