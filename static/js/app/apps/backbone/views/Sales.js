Evolucion.Views.Sales = Backbone.View.extend({
	events:{
		"click .three-buttons > input[value='Pagar']": "PaySale",
		"change #code-sale": "LoadPrices",
		"focus #code-sale, #amount-sale, #phone-number-sale, #re-phone-number-sale" : "RemoveError",
	},
	initialize : function(node){
		this.$el = node;
	},
	PaySale : function(e){
		e.preventDefault();
		var error = false;
		var product = $("#code-sale")
		var amount = $("#amount-sale")
		var phone_number = $("#phone-number-sale");
		var re_phone_number = $("#re-phone-number-sale");
		var message = $("#id_message");

		if (product.val() == ""){
			product.css("border", "1px solid red");
			error = true;
		}
		if (amount.val() == ""){
			amount.css("border", "1px solid red");
			error = true;
		}
		if ((phone_number.val() == "" || re_phone_number.val() == "") || (phone_number.val() != re_phone_number.val())){
			phone_number.css("border", "1px solid red");
			re_phone_number.css("border", "1px solid red");
			phone_number.val("");
			re_phone_number.val("");
			error = true;
		}
		if (error){
			message.find("strong").html("Falta de llenar algunos campos.");
			message.removeClass("alert-info").removeClass("alert-success").addClass("alert-danger");
			message.show()
			message.fadeOut(5000);
			return false;
		}
		message.show()
		message.removeClass("alert-success").removeClass("alert-danger").addClass("alert-info");
		message.find("strong").html("REALIZANDO PETICIÓN AL SERVIDOR DE TIEMPO AIRE ELECTRONICO <img  src='"+ app.STATIC_URL + "img/382.GIF' title='Load'></img>");

		var csrftoken = getCookie('csrftoken');
		setTimeout(function(){
			$.ajax({
				type: 'POST',
				url: 'tae/',
				async: true,
				data: {
					code_sale: product.val(),
					amount_sale: amount.val(),
					phone_number_sale: phone_number.val(),
					csrfmiddlewaretoken: csrftoken
				},
				success: function(data){
					data = JSON.parse(data);
					if(data.Confirmation == "00"){	
						message.find("strong").html("Transacción exitosa! --- Folio:" + data.Folio);
						message.removeClass("alert-info").removeClass("alert-danger").addClass("alert-success");
					}else{
						console.log(data.Description);
						message.find("strong").html(data.Description);
						message.removeClass("alert-info").removeClass("alert-success").addClass("alert-danger");
					}
					message.fadeIn(1000);
					message.fadeOut(5000);
				},
				error:function(err,e){
					console.log(err);
					console.log(e);
					message.find("strong").html(e);
					message.removeClass("alert-info").removeClass("alert-success").addClass("alert-danger");
					message.fadeIn(1000);
					message.fadeOut(5000);
				}
			});
		}, 15);
	},
	LoadPrices : function(e){
		var html_select = '<option>Monto</option>';
		var prices = $("#code-sale option:selected").attr("data-prices");
		var list_prices = prices.split(",");
		list_prices.forEach(function(obj){
			html_select += '<option value="' + obj + '">' + obj + '</option>'
		});
		$("#amount-sale").html(html_select);
	},
	RemoveError : function(e){
		$(e.target).css("border", "1px solid #ccc");
	}
});
