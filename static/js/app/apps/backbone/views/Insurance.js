Evolucion.Views.Insurance = Backbone.View.extend({
	events:{
		"click #pay-insurance" : "PayInsurance",
		"change #days-insurance" : "CalcTotal",
		"change #efective-date-insurance" : "CalcTotal"
	},
	initialize : function(node){
		this.$el = node;
	},
	PayInsurance : function(e){
		e.preventDefault();
		var error = false;
		var csrftoken = getCookie('csrftoken');
		var client_name = this.$el.find("#client-name-insurance");
		var client_name_val = this.$el.find("#client-name-insurance").val();
		var phone = this.$el.find("#phone-insurance");
		var phone_val = this.$el.find("#phone-insurance").val();
		var year = this.$el.find("#year-insurance");
		var year_val = this.$el.find("#year-insurance").val();
		var serial_number = this.$el.find("#serial-number-insurance");
		var serial_number_val = this.$el.find("#serial-number-insurance").val();
		var plate = this.$el.find("#plate-insurance");
		var plate_val = this.$el.find("#plate-insurance").val();
		var brand = this.$el.find("#brands-insurance");
		var brand_val = this.$el.find("#brands-insurance").val();
		var licence = this.$el.find("#licence-insurance");
		var licence_val = this.$el.find("#licence-insurance").val();
		var efective_date = this.$el.find("#efective-date-insurance");
		var efective_date_val = this.$el.find("#efective-date-insurance").val();
		var days = this.$el.find("#days-insurance");
		var days_val = this.$el.find("#days-insurance").val();
		var email = this.$el.find("#email-insurance");
		var email_val = this.$el.find("#email-insurance").val();
		var message = $("#id_message");

		if (client_name_val == ""){
			client_name.css("border", "1px solid red");
			error = true;
		}
		if (phone_val == ""){
			phone.css("border", "1px solid red");
			error = true;
		}
		if (year_val == ""){
			year.css("border", "1px solid red");
			error = true;
		}
		if (serial_number_val == ""){
			serial_number.css("border", "1px solid red");
			error = true;
		}
		if (plate_val == ""){
			plate.css("border", "1px solid red");
			error = true;
		}
		if (brand_val == ""){
			brand.css("border", "1px solid red");
			error = true;
		}
		if (licence_val == ""){
			licence.css("border", "1px solid red");
			error = true;
		}
		if (efective_date_val == ""){
			efective_date.css("border", "1px solid red");
			error = true;
		}
		if (days_val == ""){
			days.css("border", "1px solid red");
			error = true;
		}
		if (email_val == ""){
			email.css("border", "1px solid red");
			error = true;
		}
		if (error){
			message.find("strong").html("Falta de llenar algunos campos.");
			message.removeClass("alert-info").removeClass("alert-success").addClass("alert-danger");
			message.show()
			message.fadeOut(5000);
			return false;
		}
		
		message.show()
		message.removeClass("alert-success").removeClass("alert-danger").addClass("alert-info");
		message.find("strong").html("REALIZANDO PETICIÓN AL SERVIDOR DE TIEMPO AIRE ELECTRONICO <img  src='"+ app.STATIC_URL + "img/382.GIF' title='Load'></img>");

		setTimeout(function(){
			$.ajax({
				type: 'POST',
				url: 'process.insurance/',
				async: true,
				data: {
					phone : phone_val,
					year : year_val,
					serial_number : serial_number_val,
					plate : plate_val,
					brand : brand_val,
					licence : licence_val,
					efective_date : efective_date_val,
					days : days_val,
					email: email_val,
					csrfmiddlewaretoken: csrftoken
				},
				success: function(data){
					data = JSON.parse(data);
					if (data.Response == "1"){
						message.find("strong").html("Transacción exitosa! --- Folio:" + data.Folio);
						message.removeClass("alert-info").removeClass("alert-danger").addClass("alert-success");
					}else{
						console.log(data.Error);
						message.find("strong").html(data.Error);
						message.removeClass("alert-info").removeClass("alert-success").addClass("alert-danger");
					}
					message.fadeIn(1000);
					message.fadeOut(5000);
				},
				error:function(err,e){
					message.find("strong").html(e);
					message.removeClass("alert-info").removeClass("alert-success").addClass("alert-danger");
					message.fadeIn(1000);
					message.fadeOut(5000);
				}
			});
		}, 100);
	},
	CalcTotal : function(e){
		var select = $(e.currentTarget);
		var price = select.find("option:selected").attr("price");
		var days = parseInt(select.val());
		var effective_datetime = $("#efective-date-insurance").val();

		var temp = effective_datetime.split(" ");
		var effective_date_temp = temp[0];
		var effective_time_temp = temp[1];
		var date = effective_date_temp.split("/");
		var time = effective_time_temp.split(":");
		
		var day = parseInt(date[0]);
		var month = parseInt(date[1]);
		var year = parseInt(date[2]);

		var hour = parseInt(time[0]);
		var minute = parseInt(time[1]);

		new_date = new Date(year, month, day, hour, minute);

		new_date.setDate(new_date.getDate() + days);

		$("#expiration_date").html("Fecha Expiración: " + new_date);

		if (price != undefined){
			$("#total-insurance").html("$ " + price);
		}
		else{
			$("#total-insurance").html("$ 0.00");
		}
	}
});
