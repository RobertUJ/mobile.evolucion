Evolucion.Views.Services = Backbone.View.extend({
	events:{
		"click #pay-services": "PayServices",
		"focus #code-services, #reference-services, #amount-services" : "RemoveError"
	},
	initialize : function(node){
		this.$el = node;
	},
	PayServices : function(e){
		e.preventDefault();
		var error = false;
		var csrftoken = getCookie('csrftoken');
		var code = this.$el.find("#code-services");
		var code_val = this.$el.find("#code-services").val();
		var reference = this.$el.find("#reference-services");
		var reference_val = this.$el.find("#reference-services").val();
		var amount = this.$el.find("#amount-services");
		var amount_val = this.$el.find("#amount-services").val();
		var message = $("#id_message");
		if (code_val == ""){
			code.css("border", "1px solid red");
			error = true;
		}
		if (amount_val == ""){
			amount.css("border", "1px solid red");
			error = true;
		}
		if (reference_val == ""){
			reference.css("border", "1px solid red");
			error = true;
		}
		if (error){
			message.find("strong").html("Falta de llenar algunos campos.");
			message.removeClass("alert-info").removeClass("alert-success").addClass("alert-danger");
			message.show();
			message.fadeOut(5000);
			return false;
		}

		message.show()
		message.removeClass("alert-success").removeClass("alert-danger").addClass("alert-info");
		message.find("strong").html("REALIZANDO PETICIÓN AL SERVIDOR DE TIEMPO AIRE ELECTRONICO <img  src='"+ app.STATIC_URL + "img/382.GIF' title='Load'></img>");

		setTimeout(function(){
			$.ajax({
				type: 'POST',
				url: 'payment.for.services/',
				async: true,
				data: {
					code : code_val,
					amount : amount_val,
					reference : reference_val,
					csrfmiddlewaretoken: csrftoken
				},
				success: function(data){
					data = JSON.parse(data);
					if (data.Confirmation == "00"){
						message.find("strong").html("Transacción exitosa! --- Folio:" + data.Folio);
						message.removeClass("alert-info").removeClass("alert-danger").addClass("alert-success");
					}else{
						console.log(data.Description);
						message.find("strong").html(data.Description);
						message.removeClass("alert-info").removeClass("alert-success").addClass("alert-danger");
					}
					message.fadeIn(1000);
					message.fadeOut(5000);
				},
				error:function(err,e){
					console.log(err);
					console.log(e);
					message.find("strong").html(e);
					message.removeClass("alert-info").removeClass("alert-success").addClass("alert-danger");
					message.fadeIn(1000);
					message.fadeOut(5000);
				}
			});
		}, 15);
	},
	RemoveError : function(e){
		$(e.target).css("border", "1px solid #ccc");
	}
});