Evolucion.Routers.MainRouters = Backbone.Router.extend({
	routes: {
		"": "root",
		"services":"services",
		"insurance":"insurance",
		"reports":"reports"
	},
	initialize : function(){

		var self = this;

		var footer = $("#footer > div");

		self.on("route:root",function(){
			self.getBalance();
			nodes.Sections.find("section").hide();	
			nodes.Sections.find("#SecSale").fadeIn();
			footer.html("TIEMPO AIRE");	
		});
		
		self.on("route:services",function(){
			self.getBalance();
			nodes.Sections.find("section").hide();		
			nodes.Sections.find("#SecServices").fadeIn();
			footer.html("PAGO DE SERVICIOS");
		});

		self.on("route:insurance",function(){
			nodes.Sections.find("section").hide();		
			nodes.Sections.find("#SecInsurance").fadeIn();
			footer.html("VENTA DE SEGUROS");
			
			var csrftoken = getCookie('csrftoken');
			$.ajax({
				type: 'POST',
				url: 'get.options/',
				data: {
					csrfmiddlewaretoken: csrftoken
				},
				success: function(data){
					response = JSON.parse(data);
					if (response.Response == "1"){
						var cbo_brands = $("#brands-insurance");
						var cbo_days = $("#days-insurance");
						cbo_brands.html('<option value="">Marca...</option>');
						cbo_days.html('<option value="">Días...</option>');
						response.Brands.forEach(function(obj){
							var option = tpl.opt_brands({
								value:obj['Brand_ID'],
								text:obj['Brand_Name']
							});
							cbo_brands.append(option);
						});
						response.Products.forEach(function(obj){
							var option = tpl.opt_prices({
								value:obj['Days'],
								price:obj['Price'],
								text:obj['Days']
							});
							cbo_days.append(option);
						});
					}else{
						console.log(response.Error);
					}
				},
				error:function(err,e){
				}
			});
		});
	
		self.on("route:reports",function(){
			nodes.Sections.find("section").hide();		
			nodes.Sections.find("#SecReports").fadeIn();
			footer.html("REPORTE VENTAS");
		});
	},
	root:function(){
		console.log("Navega: Ventas");
		window.app.state = "sales";
	},
	services:function(){
		console.log("Navega: Servicios");
		window.app.state = "services";
	},
	insurance:function(){
		console.log("Navega: Seguros");
		window.app.state = "insurance";
	},
	reports:function(){
		console.log("Navega: Reportes");
		window.app.state = "reports";
	},
	getBalance : function(){
		var csrftoken = getCookie('csrftoken');
		var self = this;
		$.ajax({
			type: 'POST',
			url: 'get.balance/',
			data: {
				csrfmiddlewaretoken: csrftoken
			},
			success: function(data){
				var response = JSON.parse(data);
				if (response.Response == "1"){
					$("#balanceText").html("$ " + response.Balance);
				}else{
					$("#balanceText").html("No Disponible");
				}
			},
			error:function(err,e){
				$("#balanceText").html("No Disponible");
			}
		});
	}
});