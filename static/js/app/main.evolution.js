var InitApp = function(){
	console.log("Start App Backbone");
	// Router
	window.routers.mainRouter = new Evolucion.Routers.MainRouters();
	//init views backbone
	window.views.menu = new Evolucion.Views.MainMenu(nodes.MainMenu);
	window.views.sale = new Evolucion.Views.Sales(nodes.SecSale);
	window.views.sales_report = new Evolucion.Views.SalesReports(nodes.SecReports);
	window.views.services = new Evolucion.Views.Services(nodes.SecServices);
	window.views.insurance = new Evolucion.Views.Insurance(nodes.SecInsurance);

	Backbone.history.start({
		root : "/evolucion",
		pushState: true,
		silent : false
	});

	FastClick.attach(document.body);
	
	$(".date").datepicker({
		dateFormat: 'dd/mm/yy' 
	});

	$('.datetime').datetimepicker({
		dateFormat: 'dd/mm/yy'
	});
	$('input, textarea').placeholder();

	// Set initial info
	for (var y=2014; y > 1950; y--){
		var opt = '<option value="' + y + '">' + y + '</option>';
		$("#year-insurance").append(opt);
	}

};


// API EVOLUCIÓN
Evolucion.Api = {
	getUrl: function(){
		return "http://localhost:8000/";
	},	
	getCarriers: function(){
		var self = this;
		var url = self.getUrl();
		var carriers;
		$.ajax({
			type:"GET",
			url: "http://localhost:8000/api/carriers/",
			contentType: "text/html; charset=utf-8",
			dataType: "json",
			xhrFields: {
				withCredentials:false
			},
			success: function(data){
				var select = $("#code-sale");
				var list_option = "<option value='00'>Selecciona el proveedor...</option>"; 
				
				data.forEach(function(item){
					var option = "<option attr-Prices='"+item.prices+"' value='"+item.code+"'>"+item.name+"</option>"; 
					list_option += option;
				});
				
				select.html(list_option);
			},
			error: function(jqXHR, textStatus, ex) {
        		alert(textStatus + "," + ex + "," + jqXHR.responseText);
    		}
		});


	},getServCarriers: function(){
		var self = this;
		var url = self.getUrl();
		var jq_xhr = $.getJSON(url + "api/carriers.services/?format=json",function(data){
			return data;
		});
		jq_xhr.fail(function(){
			return [];
		});
	}
};

$(document).on("ready", InitApp);

