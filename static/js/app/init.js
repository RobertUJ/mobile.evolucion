// Set init vars to Evolucion App Cash
window.Evolucion = {};


// Backbone Config To Register App
Evolucion.Views = {};
Evolucion.Collections = {};
Evolucion.Models = {};
Evolucion.Routers = {};
Evolucion.Tools = {};


window.app = {};
window.routers = {};
window.plugs = {};
window.views = {};
window.collections = {};
window.infoSale = {};
window.nodes = {};
window.tools = {};
window.tpl = {};
window.tickets = new Array();
window.register = {};
window.data_carriers;

// Only dev mode
window.test = {};

//Templates
tpl.tr = _.template($("#tpl_tr").html());
tpl.opt_brands = _.template($("#tpl_opt_brands").html());
tpl.opt_prices = _.template($("#tpl_opt_prices").html());

//Nodes
nodes.Sections = $("#contSections");
nodes.MainMenu = $("#mainMenu");
nodes.contents = {};
nodes.SecSale = $("#SecSale");
nodes.SecServices  = $("#SecServices");
nodes.SecInsurance  = $("#SecInsurance");
nodes.SecBalance  = $("#SecBalance");
nodes.SecReports  = $("#SecReports");


// Publics vars
app.STATIC_URL = "static";



